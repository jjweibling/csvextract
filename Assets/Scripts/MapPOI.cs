//using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MapPOI : MonoBehaviour
{
    public List<MapPOIData> POIEntry;
}
public class MapPOIData
{
    public string poiname; // Index 0
    public LocationBeaconData locationBeaconData;
    public bool overlaymedia;   // Index 10
    public bool ambientmedia;   // Index 11
    public bool minigame;       // Index 12
    public bool coupon;         // Index 13
    public bool merch;          // Index 14
    public bool data;           // Index 15
}

public struct LocationBeaconData
{
    public float latitude;      // Index 20
    public float longitude;     // Index 21

}


/* - CSV Header Reference -
Location/Bldg/Business,	0
Back- ground,		    1
Ambient Sound,		    2
CTL/ Factoid,		    3
Mini-Game,		        4
Coupon/ Offer,		    5
Virt Econ/ Merch,	    6
Hidden Object,		    7
Data,			        8
Activation Point,	    9
Overlay Media,		    10
Ambient Sound,		    11
Mini-Game,		        12
Coupon/ Offer,		    13
Virt Econ/ Merch,	    14
Data,			        15
Notes,		    	    16
,			            17
,			            18
,			            19
Latitude,	    	    20
Longitude	    	    21
*/
