using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;



public class MapPOIExtract : MonoBehaviour
{

    //Define input text file. Presume Unity connection
    public TextAsset csvInput;
    public List<MapPOIData> MapPOIRoot;

    void Start()
    {
        //Extract text from assest
        string tmpString = csvInput.text;

        //Split Text into lines
        string[] csvLines = tmpString.Split('\n');

        //Index Lines
        for (int i = 1; i < csvLines.Length; i++)
        {
            //Split lines into individual entries
            string[] csvEntries = csvLines[i].Split(',');
            //New Object
            MapPOIData data = new MapPOIData();
            
            //Location Info
            data.poiname = csvEntries[0];

            Debug.Log(" Name: " + data.poiname);

            //Check for float and if store float
            //Latitude Info
            if (!string.IsNullOrWhiteSpace(csvEntries[20]))
            {
                data.locationBeaconData.latitude = float.Parse(csvEntries[20]);
                Debug.Log(" Lat: " + data.locationBeaconData.latitude.ToString());
            }

            //Longitude Info
            if (!string.IsNullOrWhiteSpace(csvEntries[21]))
            {
                Debug.Log(csvEntries[21]);
                data.locationBeaconData.longitude = float.Parse(csvEntries[21]);
                Debug.Log(" Long: " + data.locationBeaconData.longitude.ToString());
            }

            // Check boolean values represented by unknown chars
            if (csvEntries[10] != "") { data.overlaymedia = true; }
            if (csvEntries[11] != "") { data.ambientmedia = true; }
            if (csvEntries[12] != "") { data.minigame = true; }
            if (csvEntries[13] != "") { data.coupon = true; }
            if (csvEntries[14] != "") { data.merch = true; }
            if (csvEntries[15] != "") { data.data = true; }


            //Console Booleans
            Debug.Log(" Overlay Media: " + data.overlaymedia);
            Debug.Log(" Ambient Media :" + data.ambientmedia);
            Debug.Log("Overlay Media" + data.overlaymedia);
            Debug.Log("Overlay Media" + data.overlaymedia);
            Debug.Log("Overlay Media" + data.overlaymedia);
            Debug.Log("Overlay Media" + data.overlaymedia);
        }
    }
}
